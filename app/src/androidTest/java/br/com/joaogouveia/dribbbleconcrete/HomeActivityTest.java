//package br.com.joaogouveia.dribbbleconcrete;
//
//import android.support.v7.widget.RecyclerView;
//import android.test.ActivityInstrumentationTestCase2;
//
//import com.robotium.solo.Solo;
//
//import junit.framework.Assert;
//
//import br.com.joaogouveia.dribbbleconcrete.activities.DetailActivity;
//import br.com.joaogouveia.dribbbleconcrete.activities.HomeActivity;
//
//
//
///**
// * Created by joaogouveia on 17/05/15.
// */
//public class HomeActivityTest extends ActivityInstrumentationTestCase2<HomeActivity> {
//
//    private Solo mSolo;
//
//    public HomeActivityTest() {
//        super(HomeActivity.class);
//    }
//
//    public void setUp() throws Exception{
//        mSolo = new Solo(getInstrumentation(), getActivity());
//    }
//
//
//    public void testNavigationToDetails() throws Exception{
//
//        mSolo.assertCurrentActivity("Wrong Activity", HomeActivity.class);
//        mSolo.waitForView(R.id.home_shotsRecyclerView);
//        RecyclerView mList = (RecyclerView) mSolo.getView(R.id.home_shotsRecyclerView);
//        mList.getLayoutManager().getChildAt(1).performClick();
////        mSolo.clickInList(1);
//        mSolo.waitForActivity(DetailActivity.class);
//        mSolo.assertCurrentActivity("Wrong Activity", DetailActivity.class);
//        mSolo.waitForView(R.id.detail_shotImageView);
//        Assert.assertTrue(mSolo.getView(R.id.detail_shotImageView).isShown());
//
//    }
//
//    public void testNavigationToDetailsWithSwipe() throws Exception{
//
//        mSolo.assertCurrentActivity("Wrong Activity", HomeActivity.class);
//        mSolo.scrollDown();
//        mSolo.clickInList(10);
//        mSolo.waitForActivity(DetailActivity.class);
//        mSolo.assertCurrentActivity("Wrong Activity", DetailActivity.class);
//        mSolo.waitForView(R.id.detail_shotImageView);
//        Assert.assertTrue(mSolo.getView(R.id.detail_shotImageView).isShown());
//
//    }
//
//
////    public void testNavigationToAbout() throws Exception{
////
////        mSolo.assertCurrentActivity("Acitivity errada", HomeActivity.class);
////        mSolo.sendKey(Solo.MENU);
////        mSolo.clickOnText(getActivity().getString(R.string.action_about));
////        mSolo.waitForActivity(AboutActivity.class);
////        Assert.assertTrue(mSolo.searchText("Marcos"));
////
////
////    }
//
//    public void testNavigation() throws Exception{
//
//        mSolo.assertCurrentActivity("Wrong Activity", HomeActivity.class);
//        mSolo.scrollDown();
//        mSolo.clickInList(5);
//        mSolo.waitForActivity(DetailActivity.class);
//        mSolo.assertCurrentActivity("Wrong Activity", DetailActivity.class);
//        mSolo.waitForView(R.id.detail_shotImageView);
//        Assert.assertTrue(mSolo.getView(R.id.detail_shotImageView).isShown());
//        mSolo.goBack();
//        mSolo.assertCurrentActivity("Wrong Activity", HomeActivity.class);
//
//    }
//
//
//    @Override
//    protected void tearDown() throws Exception {
//        mSolo.finishOpenedActivities();
//    }
//}
