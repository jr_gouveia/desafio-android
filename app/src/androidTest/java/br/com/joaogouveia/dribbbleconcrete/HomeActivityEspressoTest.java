package br.com.joaogouveia.dribbbleconcrete;

import android.content.Intent;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.joaogouveia.dribbbleconcrete.activities.HomeActivity;
import br.com.joaogouveia.dribbbleconcrete.constants.Constants;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by joaogouveia on 18/05/15.
 */
@RunWith(AndroidJUnit4.class)
public class HomeActivityEspressoTest {

    @Rule
    public ActivityTestRule<HomeActivity> mActivityRule = new ActivityTestRule<>(
            HomeActivity.class, false, false);

    @Before
    public void init() {
        mActivityRule.launchActivity(buildIntent());
    }

    @Test
    public void testNavigation() {
        /*
        * Testa a navegação para a tela interna
        * */
        onView(isRoot()).perform(waitTime(5000));
        onView(withId(R.id.home_shotsRecyclerView)).check(matches(isDisplayed()));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(swipeUp());
        onView(isRoot()).perform(waitTime(2000));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(8, click()));
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.detail_userNameTextView)).check(matches(isCompletelyDisplayed()));
        onView(allOf(withId(R.id.detail_shotTitleTextView), withText("Linkedin"))).check(matches(isDisplayed()));
        onView(withId(R.id.detail_shotsScrollView)).perform(swipeUp());
        onView(isRoot()).perform(waitTime(1000));
        pressBack();
        /*
        * Após voltar para a lista, testa o refresh e depois vai para tela interna em outro shot
        * */
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(RecyclerViewActions.scrollToPosition(2));
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(swipeDown(), swipeDown());
        onView(isRoot()).perform(waitTime(3000));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(swipeUp());
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(6, click()));
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.detail_userNameTextView)).check(matches(isCompletelyDisplayed()));
        onView(allOf(withId(R.id.detail_shotTitleTextView), withText("New Rally Site!!")))
                .check(matches(isDisplayed()));
        onView(withId(R.id.detail_shotsScrollView)).perform(swipeUp());
        onView(isRoot()).perform(waitTime(1000));
        pressBack();
        /*
        * Por fim testa a paginação
        * */
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.home_shotsRecyclerView)).check(matches(isDisplayed()));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(RecyclerViewActions.scrollToPosition(13));
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(swipeUp());
        onView(isRoot()).perform(waitTime(2000));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(swipeUp());
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.home_shotsRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(22, click()));
        onView(isRoot()).perform(waitTime(1000));
        onView(withId(R.id.detail_userNameTextView)).check(matches(isCompletelyDisplayed()));
        onView(withId(R.id.detail_shotsScrollView)).perform(swipeUp());
        onView(isRoot()).perform(waitTime(1000));
        pressBack();
    }


    public static ViewAction waitTime(final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for a specific time during " + millis + " millis.";
            }

            @Override
            public void perform(final UiController uiController, final View view) {
                uiController.loopMainThreadUntilIdle();
                final long startTime = System.currentTimeMillis();
                final long endTime = startTime + millis;
                do {
                    uiController.loopMainThreadForAtLeast(50);

                }
                while (System.currentTimeMillis() < endTime);
            }
        };
    }

    /**
     * Used to tell how the intent that starts the activity is configured.
     *
     * @return Intent to start activity
     */
    protected Intent buildIntent() {
        Intent buildIntent = new Intent(Intent.ACTION_MAIN);
        buildIntent.putExtra(Constants.Extra.IS_TESTING, true);
        return buildIntent;
    }

}
