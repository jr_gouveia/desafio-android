package br.com.joaogouveia.dribbbleconcrete.utils;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.Display;
import android.view.WindowManager;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class Util {

    public static boolean isPreLollipop() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return true;
        }
        return false;
    }

    public static TextDrawable generateTextDrawable(@NonNull String name, int id) {

        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color1;
        if (id > 0) {
            color1 = generator.getColor(id);
        } else {
            color1 = generator.getRandomColor();
        }

        TextDrawable.IBuilder builder = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .round();
        TextDrawable ic1;
        if (name != null && name.trim().length() >= 1) {
            ic1 = builder.build(name.trim().substring(0, 1), color1);
        } else {
            ic1 = builder.build("?", color1);
        }
        return ic1;
    }

    public static int getScreenHeight(Context c) {
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

}
