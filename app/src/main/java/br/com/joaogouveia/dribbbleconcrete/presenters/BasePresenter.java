package br.com.joaogouveia.dribbbleconcrete.presenters;

import com.google.gson.GsonBuilder;

import br.com.joaogouveia.dribbbleconcrete.application.DesafioApplication;
import br.com.joaogouveia.dribbbleconcrete.constants.Constants;
import br.com.joaogouveia.dribbbleconcrete.rest.WebServiceAPI;
import br.com.joaogouveia.dribbbleconcrete.rest.WebServiceMockedAPI;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class BasePresenter {

    protected static final String TAG = "BasePresenter";

    protected WebServiceAPI webService;
    protected WebServiceMockedAPI webServiceMocked;

    protected BasePresenter() {
        GsonBuilder b = new GsonBuilder();
        b.setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        RestAdapter.Builder builder = new RestAdapter.Builder();
        if (DesafioApplication.getInstance().getIsTesting()) {
            builder.setEndpoint(Constants.API_HOST_MOCK);
        } else {
            builder.setEndpoint(Constants.API_HOST);
        }

        RestAdapter webServiceAPIRest = builder.setConverter(new GsonConverter(b.create()))
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();
        if (DesafioApplication.getInstance().getIsTesting()) {
            webServiceMocked = webServiceAPIRest.create(WebServiceMockedAPI.class);
        } else {
            webService = webServiceAPIRest.create(WebServiceAPI.class);
        }
    }

}
