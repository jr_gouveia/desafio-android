package br.com.joaogouveia.dribbbleconcrete.rest;

import java.util.ArrayList;

import br.com.joaogouveia.dribbbleconcrete.constants.Constants;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by joaogouveia on 16/05/15.
 */
public interface WebServiceMockedAPI{
    @Headers({"JsonStub-User-Key: f4bff83d-92fb-4863-bcd5-214d285b876e",
            "JsonStub-Project-Key: 6ba72c11-86d4-4a7e-9b2b-93966453186c",
            "Content-Type: application/json"})
    @GET("/shots")
    void getPopularShots(@Query(Constants.RequestKeys.PAGE) int page, @Query(Constants.RequestKeys.PER_PAGE) int perPage, Callback<ArrayList<Shot>> cb);

    @Headers({"JsonStub-User-Key: f4bff83d-92fb-4863-bcd5-214d285b876e",
            "JsonStub-Project-Key: 6ba72c11-86d4-4a7e-9b2b-93966453186c",
            "Content-Type: application/json"})
    @GET("/shots")
    void getPopularGifShots(@Query(Constants.RequestKeys.PAGE) int page, @Query(Constants.RequestKeys.PER_PAGE) int perPage, @Query(Constants.RequestKeys.LIST) String list, Callback<ArrayList<Shot>> cb);

    @Headers({"JsonStub-User-Key: f4bff83d-92fb-4863-bcd5-214d285b876e",
            "JsonStub-Project-Key: 6ba72c11-86d4-4a7e-9b2b-93966453186c",
            "Content-Type: application/json"})
    @GET("/shots/{shotId}")
    void getShotById(@Path("shotId") int shotId, Callback<Shot> cb);
}
