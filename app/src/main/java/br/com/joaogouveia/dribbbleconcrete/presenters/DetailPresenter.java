package br.com.joaogouveia.dribbbleconcrete.presenters;

import android.util.Log;

import br.com.joaogouveia.dribbbleconcrete.fragments.DetailFragment;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class DetailPresenter extends BasePresenter {

    protected static final String TAG = "DetailPresenter";
    private static DetailPresenter mPresenter;

    private DetailFragment mView;
    private Shot mShot;
    private RetrofitError mRetrofitError;

    protected DetailPresenter() {
        super();
    }

    public static DetailPresenter getInstance() {
        if (mPresenter == null) {
            mPresenter = new DetailPresenter();
        }
        return mPresenter;
    }

    public void getShotById(int id) {
        webService.getShotById(id, new Callback<Shot>() {
            @Override
            public void success(Shot shot, Response response) {
                Log.d(TAG, "shot recebido " + shot.title + " de " + response.getUrl());
                mShot = shot;
                publish();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                mRetrofitError = retrofitError;
                publish();
            }
        });
    }


    public void onTakeView(DetailFragment view) {
        this.mView = view;
            publish();
    }

    private void publish() {
        if (mView != null) {
            if (mShot != null) {
                mView.onItemNext(mShot);
                mShot = null;
            }else if (mRetrofitError != null) {
                mView.onItemError(mRetrofitError);
                mRetrofitError = null;
            }
        }
    }

}
