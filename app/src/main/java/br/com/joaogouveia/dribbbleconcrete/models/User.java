package br.com.joaogouveia.dribbbleconcrete.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class User implements Parcelable {

    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("username")
    public String username;
    @SerializedName("html_url")
    public String htmlUrl;
    @SerializedName("avatar_url")
    public String avatarUrl;
    @SerializedName("bio")
    public String bio;
    @SerializedName("location")
    public String location;
    @SerializedName("buckets_count")
    public int bucketsCount;
    @SerializedName("comments_received_count")
    public int commentsReceivedCount;
    @SerializedName("followers_count")
    public int followersCount;
    @SerializedName("followings_count")
    public int followingsCount;
    @SerializedName("likes_count")
    public int likesCount;
    @SerializedName("likes_received_count")
    public int likesReceivedCount;
    @SerializedName("projects_count")
    public int projectsCount;
    @SerializedName("rebounds_received_count")
    public int reboundsReceivedCount;
    @SerializedName("shots_count")
    public int shotsCount;
    @SerializedName("teams_count")
    public int teamsCount;
    @SerializedName("can_upload_shot")
    public boolean canUploadShot;
    @SerializedName("type")
    public String type;
    @SerializedName("pro")
    public boolean pro;
    @SerializedName("buckets_url")
    public String bucketsUrl;
    @SerializedName("followers_url")
    public String followersUrl;
    @SerializedName("following_url")
    public String followingUrl;
    @SerializedName("likes_url")
    public String likesUrl;
    @SerializedName("projects_url")
    public String projectsUrl;
    @SerializedName("shots_url")
    public String shotsUrl;
    @SerializedName("teams_url")
    public String teamsUrl;
    @SerializedName("created_at")
    public Date createdAt;
    @SerializedName("updated_at")
    public Date updatedAt;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.username);
        dest.writeString(this.htmlUrl);
        dest.writeString(this.avatarUrl);
        dest.writeString(this.bio);
        dest.writeString(this.location);
        dest.writeInt(this.bucketsCount);
        dest.writeInt(this.commentsReceivedCount);
        dest.writeInt(this.followersCount);
        dest.writeInt(this.followingsCount);
        dest.writeInt(this.likesCount);
        dest.writeInt(this.likesReceivedCount);
        dest.writeInt(this.projectsCount);
        dest.writeInt(this.reboundsReceivedCount);
        dest.writeInt(this.shotsCount);
        dest.writeInt(this.teamsCount);
        dest.writeByte(canUploadShot ? (byte) 1 : (byte) 0);
        dest.writeString(this.type);
        dest.writeByte(pro ? (byte) 1 : (byte) 0);
        dest.writeString(this.bucketsUrl);
        dest.writeString(this.followersUrl);
        dest.writeString(this.followingUrl);
        dest.writeString(this.likesUrl);
        dest.writeString(this.projectsUrl);
        dest.writeString(this.shotsUrl);
        dest.writeString(this.teamsUrl);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
        dest.writeLong(updatedAt != null ? updatedAt.getTime() : -1);
    }

    public User() {
    }

    private User(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.username = in.readString();
        this.htmlUrl = in.readString();
        this.avatarUrl = in.readString();
        this.bio = in.readString();
        this.location = in.readString();
        this.bucketsCount = in.readInt();
        this.commentsReceivedCount = in.readInt();
        this.followersCount = in.readInt();
        this.followingsCount = in.readInt();
        this.likesCount = in.readInt();
        this.likesReceivedCount = in.readInt();
        this.projectsCount = in.readInt();
        this.reboundsReceivedCount = in.readInt();
        this.shotsCount = in.readInt();
        this.teamsCount = in.readInt();
        this.canUploadShot = in.readByte() != 0;
        this.type = in.readString();
        this.pro = in.readByte() != 0;
        this.bucketsUrl = in.readString();
        this.followersUrl = in.readString();
        this.followingUrl = in.readString();
        this.likesUrl = in.readString();
        this.projectsUrl = in.readString();
        this.shotsUrl = in.readString();
        this.teamsUrl = in.readString();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
