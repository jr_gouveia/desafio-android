package br.com.joaogouveia.dribbbleconcrete.application;

import android.app.Application;

/**
 * Created by joaogouveia on 5/18/15.
 */
public class DesafioApplication extends Application {

    boolean isTesting = false;
    static DesafioApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }

    public static DesafioApplication getInstance(){
        return INSTANCE;
    }

    public void setIsTesting(boolean isTesting){
        this.isTesting = isTesting;
    }

    public boolean getIsTesting(){
        return this.isTesting;
    }
}
