package br.com.joaogouveia.dribbbleconcrete.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import br.com.joaogouveia.dribbbleconcrete.R;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import icepick.Icepick;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class BaseActivity extends AppCompatActivity {

    @InjectView(R.id.base_toolbar)
    protected Toolbar toolbar;
    @Optional
    @InjectView(R.id.base_frameLayout)
    FrameLayout mFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(android.view.Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().requestFeature(android.view.Window.FEATURE_ACTIVITY_TRANSITIONS);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.inject(this);
    }

    protected void configureBackToolbar() {
        configureToolbar();
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void configureToolbar() {
        setSupportActionBar(toolbar);

    }

    public void setToolbarTitle(String mTitle) {
        getSupportActionBar().setTitle(mTitle);
    }

    public void setToolbarTitle(int mTitleRes) {
        getSupportActionBar().setTitle(mTitleRes);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }
}
