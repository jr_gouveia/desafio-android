package br.com.joaogouveia.dribbbleconcrete.presenters;

import android.util.Log;

import java.util.ArrayList;

import br.com.joaogouveia.dribbbleconcrete.application.DesafioApplication;
import br.com.joaogouveia.dribbbleconcrete.constants.Constants;
import br.com.joaogouveia.dribbbleconcrete.fragments.HomeFragment;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class HomePresenter extends BasePresenter {

    protected static final String TAG = "HomePresenter";
    private static HomePresenter mPresenter;

    private HomeFragment mView;
    private ArrayList<Shot> mShots;
    private RetrofitError mRetrofitError;
    
    protected HomePresenter() {
        super();
    }

    public static HomePresenter getInstance() {
        if (mPresenter == null) {
            mPresenter = new HomePresenter();
        }
        return mPresenter;
    }

    public void getPopularShots(int page) {
                if (DesafioApplication.getInstance().getIsTesting()) {
                    webServiceMocked.getPopularShots(page, Constants.Parameters.PER_PAGE, new Callback<ArrayList<Shot>>() {
                        //        webService.getPopularGifShots(page, Constants.Parameters.PER_PAGE, Constants.Parameters.ANIMATED, new Callback<ArrayList<Shot>>() {
                        @Override
                        public void success(ArrayList<Shot> shots, Response response) {
                            Log.d(TAG, "quantidade de shots recebidos " + shots.size() + " de " + response.getUrl());
                            mShots = shots;
                            publish();
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            mRetrofitError = retrofitError;
                            publish();
                        }
                    });
                }else {
                    webService.getPopularShots(page, Constants.Parameters.PER_PAGE, new Callback<ArrayList<Shot>>() {
                        //        webService.getPopularGifShots(page, Constants.Parameters.PER_PAGE, Constants.Parameters.ANIMATED, new Callback<ArrayList<Shot>>() {
                        @Override
                        public void success(ArrayList<Shot> shots, Response response) {
                            Log.d(TAG, "quantidade de shots recebidos " + shots.size() + " de " + response.getUrl());
                            mShots = shots;
                            publish();
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            mRetrofitError = retrofitError;
                            publish();
                        }
                    });
                }
    }

    public void onTakeView(HomeFragment view) {
        this.mView = view;
            publish();
    }

    private void publish() {
        if (mView != null) {
            if (mShots != null) {
                mView.onItemsNext(mShots);
                mShots = null;
            }else if (mRetrofitError != null) {
                mView.onItemsError(mRetrofitError);
                mRetrofitError = null;
            }
        }
    }

}
