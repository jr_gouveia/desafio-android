package br.com.joaogouveia.dribbbleconcrete.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;

import br.com.joaogouveia.dribbbleconcrete.R;
import br.com.joaogouveia.dribbbleconcrete.constants.Constants;
import br.com.joaogouveia.dribbbleconcrete.fragments.DetailFragment;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import icepick.Icicle;

public class DetailActivity extends BaseActivity {

    public static final String FRAGMENT_TAG = "fragment:detail";
    DetailFragment fragment;
    @Icicle
    Shot mShot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.Extra.SHOT)) {
                mShot = extras.getParcelable(Constants.Extra.SHOT);
            }
        }

        configureBackToolbar();
        setToolbarTitle(mShot.title);

        fragment = (DetailFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if (fragment == null) {
            fragment = DetailFragment.newInstance(mShot);
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.base_frameLayout, fragment, FRAGMENT_TAG).commit();
    }

}
