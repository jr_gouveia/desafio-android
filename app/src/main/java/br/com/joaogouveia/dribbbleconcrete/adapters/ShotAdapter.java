package br.com.joaogouveia.dribbbleconcrete.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.joaogouveia.dribbbleconcrete.R;
import br.com.joaogouveia.dribbbleconcrete.activities.BaseActivity;
import br.com.joaogouveia.dribbbleconcrete.models.ItemList;
import br.com.joaogouveia.dribbbleconcrete.models.Loading;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import br.com.joaogouveia.dribbbleconcrete.viewholders.LoadingViewHolder;
import br.com.joaogouveia.dribbbleconcrete.viewholders.ShotViewHolder;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class ShotAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int SHOT = 0;
    public static final int LOADING = 1;
    ArrayList<ItemList> mShotsList = new ArrayList<>();
    BaseActivity mContext;

    public ShotAdapter(@NonNull BaseActivity mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (mShotsList.get(position) instanceof Loading) {
            return LOADING;
        }
        return SHOT;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v ;
        if(viewType == LOADING) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_progress, parent, false);
            return new LoadingViewHolder(v);
        }else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.shot_item_layout, parent, false);
            return new ShotViewHolder(v, mContext);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if(viewHolder instanceof ShotViewHolder) {
            Shot mShot = (Shot) mShotsList.get(i);
            ((ShotViewHolder)viewHolder).populate(mShot);
        }
    }

    @Override
    public int getItemCount() {
        return mShotsList.size();
    }

    public void addItems(ArrayList<Shot> mShots) {
        removeLoading();
        int initialPos = mShotsList.size();
        mShotsList.addAll(mShots);
        notifyItemRangeInserted(initialPos, mShots.size());
    }

    public void removeLoading(){
        int lastPos = mShotsList.size()-1;
        if(lastItemIsLoading()){
            mShotsList.remove(lastPos);
            notifyItemRemoved(lastPos);
        }
    }

    public void addLoading(){
        if(!lastItemIsLoading()) {
            int pos = mShotsList.size();
            mShotsList.add(new Loading());
            notifyItemInserted(pos);
        }
    }

    private boolean lastItemIsLoading(){
        int lastPos = mShotsList.size()-1;
        if(lastPos > 0 && mShotsList.get(lastPos) instanceof Loading){
            return true;
        }
        return false;
    }


}
