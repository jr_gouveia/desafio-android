package br.com.joaogouveia.dribbbleconcrete.viewholders;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.joaogouveia.dribbbleconcrete.R;
import br.com.joaogouveia.dribbbleconcrete.activities.BaseActivity;
import br.com.joaogouveia.dribbbleconcrete.activities.DetailActivity;
import br.com.joaogouveia.dribbbleconcrete.constants.Constants;
import br.com.joaogouveia.dribbbleconcrete.fragments.DetailFragment;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class ShotViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    @InjectView(R.id.shotItem_imageView)
    ImageView mShotItemImageView;
    @InjectView(R.id.shotItem_viewsCountTextView)
    TextView mShotItemViewsCountTextView;
    @InjectView(R.id.shotItem_titleTextView)
    TextView mShotItemTitleTextView;
    @InjectView(R.id.shotItem_imageFrame)
    FrameLayout mShotItemImageFrame;
    @InjectView(R.id.shotItem_titleBackgroundLayout)
    RelativeLayout mShotItemTitleBackgroundLayout;
    BaseActivity mContext;

    Shot mShot;

    public ShotViewHolder(View view, @NonNull BaseActivity mContext) {
        super(view);
        this.mContext = mContext;
        ButterKnife.inject(this, view);
        mShotItemImageFrame.setOnClickListener(this);
    }

    public void populate(@NonNull Shot mShot) {
        this.mShot = mShot;
        mShotItemTitleTextView.setText(mShot.title);
        mShotItemViewsCountTextView.setText(String.valueOf(mShot.viewsCount));
        Glide.with(mContext).load(mShot.images.normal).override(400, 300)
                .fitCenter().placeholder(R.drawable.photo_placeholder)
                .error(R.drawable.photo_placeholder).into(mShotItemImageView);
    }

    @Override
    public void onClick(View view) {
        Intent detailIntent = new Intent(mContext, DetailActivity.class);
        detailIntent.putExtra(Constants.Extra.SHOT, mShot);

        ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                mContext,
                new Pair<View, String>(mShotItemImageView,
                        DetailFragment.VIEW_NAME_SHOT_IMAGE),
                new Pair<View, String>(mShotItemTitleTextView,
                        DetailFragment.VIEW_NAME_SHOT_TITLE),
                new Pair<View, String>(mShotItemViewsCountTextView,
                        DetailFragment.VIEW_NAME_SHOT_VIEWS),
                new Pair<View, String>(mContext.getToolbar(),
                        DetailFragment.VIEW_NAME_SHOT_TOOLBAR));

        // Now we can start the Activity, providing the activity options as a bundle
        ActivityCompat.startActivity(mContext, detailIntent, activityOptions.toBundle());
    }
}