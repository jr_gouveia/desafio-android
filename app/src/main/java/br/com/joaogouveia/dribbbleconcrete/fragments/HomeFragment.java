package br.com.joaogouveia.dribbbleconcrete.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.util.ArrayList;

import br.com.joaogouveia.dribbbleconcrete.R;
import br.com.joaogouveia.dribbbleconcrete.activities.BaseActivity;
import br.com.joaogouveia.dribbbleconcrete.adapters.ShotAdapter;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import br.com.joaogouveia.dribbbleconcrete.presenters.HomePresenter;
import butterknife.ButterKnife;
import butterknife.InjectView;
import icepick.Icicle;
import retrofit.RetrofitError;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, ObservableScrollViewCallbacks {

    public static final String TAG = "HomeFragment";
    protected static HomePresenter mPresenter;
    @InjectView(R.id.home_shotsRecyclerView)
    ObservableRecyclerView mHomeShotsRecyclerView;
    @InjectView(R.id.home_swipeLayout)
    SwipeRefreshLayout mHomeSwipeLayout;
    @InjectView(R.id.home_loading)
    FrameLayout mHomeLoading;
    @Icicle
    int mPage = 1;
    @Icicle
    ArrayList<Shot> mShotsList;
    @Icicle
    int scrollPosition = 0;
    @InjectView(R.id.home_error)
    FrameLayout mHomeError;
    @InjectView(R.id.error_textView)
    TextView mErrorTextView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ShotAdapter mAdapter;
    private boolean isPagging = false;
    private boolean alreadyShowingLastItem = false;
    private int lastItem = 0;


    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainContent = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.inject(this, mainContent);

        mHomeShotsRecyclerView.setHasFixedSize(true);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mLayoutManager = new LinearLayoutManager(getActivity());
            mHomeShotsRecyclerView.setLayoutManager(mLayoutManager);
        } else {
            mLayoutManager = new GridLayoutManager(getActivity(), 2);
            mHomeShotsRecyclerView.setLayoutManager(mLayoutManager);
        }
        mHomeShotsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mHomeShotsRecyclerView.setScrollViewCallbacks(this);

        configureSwipeLayout();
        return mainContent;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mPresenter == null) {
            mPresenter = HomePresenter.getInstance();
        }
        mPresenter.onTakeView(this);

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mShotsList != null) {
            if (mAdapter == null || mAdapter.getItemCount() == 0) {
                onItemsNext(mShotsList);
            }
        } else {
            showLoading();
            mPresenter.getPopularShots(mPage);
        }
    }

    public void onItemsNext(ArrayList<Shot> mShots) {
        if (isPagging) {
            this.mShotsList.addAll(mShots);
            mAdapter.addItems(mShots);
        } else {
            if (mAdapter == null) {
                showContent();
            }
            this.mShotsList = mShots;
            mAdapter = new ShotAdapter((BaseActivity) getActivity());
            mAdapter.addItems(mShots);
            mHomeShotsRecyclerView.setAdapter(mAdapter);
        }
        dismissLoading();
    }

    public void onItemsError(RetrofitError mRetrofitError) {
        if (mAdapter != null) {
            mAdapter.removeLoading();
            showSnackbarError(mRetrofitError);
        } else {
            showError(mRetrofitError);
        }

        dismissLoading();
        if (isPagging && mPage > 1) {
            mPage -= 1;
        }
    }

    private void dismissLoading() {
        isPagging = false;
        if (mHomeSwipeLayout.isRefreshing()) {
            mHomeSwipeLayout.setRefreshing(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onTakeView(null);
        ButterKnife.reset(this);
    }

    @Override
    public void onRefresh() {
        isPagging = false;
        mPage = 1;
        mPresenter.getPopularShots(mPage);
    }

    public void configureSwipeLayout() {
        mHomeSwipeLayout.setOnRefreshListener(this);
        mHomeSwipeLayout.setColorSchemeResources(R.color.material_color_control,
                R.color.primary_material,
                R.color.material_color_control,
                R.color.primary_material);
        mHomeSwipeLayout.setSize(SwipeRefreshLayout.LARGE);
    }

    private void showLoading() {
        mHomeLoading.setVisibility(View.VISIBLE);
        mHomeError.setVisibility(View.GONE);
        mHomeShotsRecyclerView.setVisibility(View.GONE);
    }

    private void showError(RetrofitError mRetrofitError) {
        mHomeError.setVisibility(View.VISIBLE);
        switch (mRetrofitError.getKind()) {
            case HTTP:
                Log.e(TAG, "falha de HTTP: ", mRetrofitError.fillInStackTrace());
                mErrorTextView.setText(R.string.message_error_http);
                break;
            case NETWORK:
                Log.e(TAG, "falha de conexão: ", mRetrofitError.fillInStackTrace());
                mErrorTextView.setText(R.string.message_error_network);
                break;
            default:
                Log.e(TAG, "falha desconhecida: ", mRetrofitError.fillInStackTrace());
                mErrorTextView.setText(R.string.message_error_default);
                break;
        }
        mHomeLoading.setVisibility(View.GONE);
        mHomeShotsRecyclerView.setVisibility(View.GONE);
    }

    private void showContent() {
        mHomeShotsRecyclerView.setVisibility(View.VISIBLE);
        mHomeError.setVisibility(View.GONE);
        mHomeLoading.setVisibility(View.GONE);
    }

    private void showSnackbarError(RetrofitError mRetrofitError) {
        switch (mRetrofitError.getKind()) {
            case HTTP:
                Log.e(TAG, "falha de HTTP: ", mRetrofitError.fillInStackTrace());
                SnackbarManager.show(Snackbar.with(getActivity())
                        .text(R.string.message_error_http), getActivity());
                break;
            case NETWORK:
                Log.e(TAG, "falha de conexão: ", mRetrofitError.fillInStackTrace());
                SnackbarManager.show(Snackbar.with(getActivity())
                        .text(R.string.message_error_network), getActivity());
                break;
            default:
                Log.e(TAG, "falha desconhecida: ", mRetrofitError.fillInStackTrace());
                SnackbarManager.show(Snackbar.with(getActivity())
                        .text(R.string.message_error_default), getActivity());
                break;
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        scrollPosition = scrollY;
        int tempLastVisibleItemPosition;
        if (mLayoutManager instanceof LinearLayoutManager) {
            tempLastVisibleItemPosition = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        } else {
            tempLastVisibleItemPosition = ((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        }

        if (lastItem != tempLastVisibleItemPosition) {
            lastItem = tempLastVisibleItemPosition;
            if (tempLastVisibleItemPosition == mHomeShotsRecyclerView.getAdapter().getItemCount() - 1) {
                if (!isPagging && !alreadyShowingLastItem) {
                    alreadyShowingLastItem = true;
                    isPagging = true;
                    mPage += 1;
                    mAdapter.addLoading();
                    mPresenter.getPopularShots(mPage);
                }
            } else {
                alreadyShowingLastItem = false;
            }
        }

    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }
}
