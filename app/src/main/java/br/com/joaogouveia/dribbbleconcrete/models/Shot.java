package br.com.joaogouveia.dribbbleconcrete.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by joaogouveia on 15/05/15.
 */
public class Shot extends ItemList implements Parcelable {

    @SerializedName("id")
    public int id;
    @SerializedName("title")
    public String title;
    @SerializedName("description")
    public String description;
    @SerializedName("height")
    public int height;
    @SerializedName("width")
    public int width;
    @SerializedName("images")
    public Images images;
    @SerializedName("views_count")
    public int viewsCount;
    @SerializedName("likes_count")
    public int likesCount;
    @SerializedName("comments_count")
    public int commentsCount;
    @SerializedName("attachments_count")
    public int attachmentsCount;
    @SerializedName("rebounds_count")
    public int reboundsCount;
    @SerializedName("buckets_count")
    public int bucketsCount;
    @SerializedName("created_at")
    public Date createdAt;
    @SerializedName("updated_at")
    public Date updatedAt;
    @SerializedName("html_url")
    public String htmlUrl;
    @SerializedName("attachments_url")
    public String attachmentsUrl;
    @SerializedName("buckets_url")
    public String bucketsUrl;
    @SerializedName("comments_url")
    public String commentsUrl;
    @SerializedName("likes_url")
    public String likesUrl;
    @SerializedName("projects_url")
    public String projectsUrl;
    @SerializedName("rebounds_url")
    public String reboundsUrl;
    @SerializedName("tags")
    public String[] tags;
    @SerializedName("user")
    public User user;
    @SerializedName("team")
    public Team team;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeInt(this.height);
        dest.writeInt(this.width);
        dest.writeParcelable(this.images, 0);
        dest.writeInt(this.viewsCount);
        dest.writeInt(this.likesCount);
        dest.writeInt(this.commentsCount);
        dest.writeInt(this.attachmentsCount);
        dest.writeInt(this.reboundsCount);
        dest.writeInt(this.bucketsCount);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
        dest.writeLong(updatedAt != null ? updatedAt.getTime() : -1);
        dest.writeString(this.htmlUrl);
        dest.writeString(this.attachmentsUrl);
        dest.writeString(this.bucketsUrl);
        dest.writeString(this.commentsUrl);
        dest.writeString(this.likesUrl);
        dest.writeString(this.projectsUrl);
        dest.writeString(this.reboundsUrl);
        dest.writeStringArray(this.tags);
        dest.writeParcelable(this.user, 0);
        dest.writeParcelable(this.team, 0);
    }

    public Shot() {
    }

    private Shot(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.height = in.readInt();
        this.width = in.readInt();
        this.images = in.readParcelable(Images.class.getClassLoader());
        this.viewsCount = in.readInt();
        this.likesCount = in.readInt();
        this.commentsCount = in.readInt();
        this.attachmentsCount = in.readInt();
        this.reboundsCount = in.readInt();
        this.bucketsCount = in.readInt();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        this.htmlUrl = in.readString();
        this.attachmentsUrl = in.readString();
        this.bucketsUrl = in.readString();
        this.commentsUrl = in.readString();
        this.likesUrl = in.readString();
        this.projectsUrl = in.readString();
        this.reboundsUrl = in.readString();
        this.tags = in.createStringArray();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.team = in.readParcelable(Team.class.getClassLoader());
    }

    public static final Parcelable.Creator<Shot> CREATOR = new Parcelable.Creator<Shot>() {
        public Shot createFromParcel(Parcel source) {
            return new Shot(source);
        }

        public Shot[] newArray(int size) {
            return new Shot[size];
        }
    };
}
