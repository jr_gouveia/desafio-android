package br.com.joaogouveia.dribbbleconcrete.rest;

import java.util.ArrayList;

import br.com.joaogouveia.dribbbleconcrete.constants.Constants;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by joaogouveia on 16/05/15.
 */
public interface WebServiceAPI{
    @Headers("Authorization: Bearer 0e9849606f60f9166599d7b3c1652024079b2185098abc5c92f07a2e35b2962a")
    @GET("/shots")
    void getPopularShots(@Query(Constants.RequestKeys.PAGE) int page, @Query(Constants.RequestKeys.PER_PAGE) int perPage, Callback<ArrayList<Shot>> cb);

    @Headers("Authorization: Bearer 0e9849606f60f9166599d7b3c1652024079b2185098abc5c92f07a2e35b2962a")
    @GET("/shots")
    void getPopularGifShots(@Query(Constants.RequestKeys.PAGE) int page, @Query(Constants.RequestKeys.PER_PAGE) int perPage, @Query(Constants.RequestKeys.LIST) String list, Callback<ArrayList<Shot>> cb);

    @Headers("Authorization: Bearer 0e9849606f60f9166599d7b3c1652024079b2185098abc5c92f07a2e35b2962a")
    @GET("/shots/{shotId}")
    void getShotById(@Path("shotId") int shotId, Callback<Shot> cb);
}
