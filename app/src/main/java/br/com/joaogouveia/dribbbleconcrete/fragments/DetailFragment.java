package br.com.joaogouveia.dribbbleconcrete.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.graphics.Palette;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import br.com.joaogouveia.dribbbleconcrete.R;
import br.com.joaogouveia.dribbbleconcrete.activities.BaseActivity;
import br.com.joaogouveia.dribbbleconcrete.constants.Constants;
import br.com.joaogouveia.dribbbleconcrete.models.Shot;
import br.com.joaogouveia.dribbbleconcrete.presenters.DetailPresenter;
import br.com.joaogouveia.dribbbleconcrete.utils.CropCircleTransformation;
import br.com.joaogouveia.dribbbleconcrete.utils.Util;
import butterknife.ButterKnife;
import butterknife.InjectView;
import icepick.Icicle;
import retrofit.RetrofitError;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class DetailFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final String VIEW_NAME_SHOT_IMAGE = "detail:shot:image";
    public static final String VIEW_NAME_SHOT_TITLE = "detail:shot:title";
    public static final String VIEW_NAME_SHOT_VIEWS = "detail:shot:views";
    public static final String VIEW_NAME_SHOT_TOOLBAR = "detail:shot:toolbar";
    public static final String TAG = "DetailFragment";

    protected static DetailPresenter mPresenter;

    @Icicle
    Shot mShot;
    @InjectView(R.id.detail_shotImageView)
    ImageView mDetailShotImageView;
    @InjectView(R.id.detail_shotViewsCountTextView)
    TextView mDetailShotViewsCountTextView;
    @InjectView(R.id.detail_shotTitleTextView)
    TextView mDetailShotTitleTextView;
    @InjectView(R.id.detail_shotRootLayout)
    RelativeLayout mDetailShotRootLayout;
    @InjectView(R.id.detail_userImageView)
    ImageView mDetailUserImageView;
    @InjectView(R.id.detail_userNameTextView)
    TextView mDetailUserNameTextView;
    @InjectView(R.id.detail_shotDescriptionTextView)
    TextView mDetailShotDescriptionTextView;
    @InjectView(R.id.detail_userRootLayout)
    RelativeLayout mDetailUserRootLayout;
    @InjectView(R.id.detail_shotsScrollView)
    ObservableScrollView mDetailShotsScrollView;
    @InjectView(R.id.detail_swipeLayout)
    SwipeRefreshLayout mDetailSwipeLayout;
    @InjectView(R.id.detail_shotTitleBackgroundLayout)
    RelativeLayout mDetailShotTitleBackgroundLayout;
    private BaseActivity mContext;


    public static DetailFragment newInstance(Shot mShot) {
        DetailFragment fragment = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.Extra.SHOT, mShot);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(Constants.Extra.SHOT)) {
                this.mShot = args.getParcelable(Constants.Extra.SHOT);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainContent = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.inject(this, mainContent);

        ViewCompat.setTransitionName(mDetailShotImageView, VIEW_NAME_SHOT_IMAGE);
        ViewCompat.setTransitionName(mDetailShotTitleTextView, VIEW_NAME_SHOT_TITLE);
        ViewCompat.setTransitionName(mDetailShotViewsCountTextView, VIEW_NAME_SHOT_VIEWS);
        mContext = (BaseActivity) getActivity();
        ViewCompat.setTransitionName(mContext.getToolbar(), VIEW_NAME_SHOT_TOOLBAR);

        configureSwipeLayout();

        populateLayout();

        return mainContent;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mPresenter == null) {
            mPresenter = DetailPresenter.getInstance();
        }
        mPresenter.onTakeView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mShot != null) {
            onItemNext(mShot);
        }
    }

    public void onItemNext(Shot mShot) {
        this.mShot = mShot;
        populateLayout();
        dismissLoading();
    }

    public void onItemError(RetrofitError mRetrofitError) {
        showSnackbarError(mRetrofitError);
        dismissLoading();

    }

    private void populateLayout() {
        mDetailShotTitleTextView.setText(mShot.title);
        mDetailShotViewsCountTextView.setText(String.valueOf(mShot.viewsCount));
        mDetailUserNameTextView.setText(mShot.user.name);
        if (mShot.description != null) {
            mDetailShotDescriptionTextView.setText(Html.fromHtml(mShot.description));
            mDetailShotDescriptionTextView.setMovementMethod(LinkMovementMethod.getInstance());
        }
        Glide.with(mContext.getApplicationContext()).load(mShot.images.normal).override(400, 300)
                .fitCenter().placeholder(R.drawable.photo_placeholder)
                .error(R.drawable.photo_placeholder).into(mDetailShotImageView);
        TextDrawable avatar = Util.generateTextDrawable(mShot.user.name, mShot.user.id);
        Glide.with(mContext).load(mShot.user.avatarUrl).override(160, 160)
                .fitCenter().bitmapTransform(new CropCircleTransformation(Glide.get(mContext)
                .getBitmapPool())).placeholder(avatar)
                .error(avatar).into(mDetailUserImageView);
        Glide.with(mContext.getApplicationContext()).load(mShot.images.normal).asBitmap().into(new SimpleTarget<Bitmap>(400, 300) {
            @Override
            public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                    public void onGenerated(Palette p) {
                        int toolbarColor = p.getDarkVibrantColor(mContext.getResources().getColor(R.color.primary_material));
                        mContext.getToolbar().setBackgroundColor(toolbarColor);
                        if (!Util.isPreLollipop()) {
                            float[] hsv = new float[3];
                            Color.colorToHSV(toolbarColor, hsv);
                            hsv[2] *= 0.8f;
                            int statusBarColor = Color.HSVToColor(hsv);
                            mContext.getWindow().setStatusBarColor(statusBarColor);
                        }
                    }
                });
            }
        });

    }

    private void dismissLoading() {
        if (mDetailSwipeLayout.isRefreshing()) {
            mDetailSwipeLayout.setRefreshing(false);
        }
    }

    private void showSnackbarError(RetrofitError mRetrofitError) {
        switch (mRetrofitError.getKind()) {
            case HTTP:
                Log.e(TAG, "falha de HTTP: ", mRetrofitError.fillInStackTrace());
                SnackbarManager.show(Snackbar.with(mContext)
                        .text(R.string.message_error_http), mContext);
                break;
            case NETWORK:
                Log.e(TAG, "falha de conexão: ", mRetrofitError.fillInStackTrace());
                SnackbarManager.show(Snackbar.with(mContext)
                        .text(R.string.message_error_network), mContext);
                break;
            default:
                Log.e(TAG, "falha desconhecida: ", mRetrofitError.fillInStackTrace());
                SnackbarManager.show(Snackbar.with(mContext)
                        .text(R.string.message_error_default), mContext);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onTakeView(null);
        ButterKnife.reset(this);
    }

    @Override
    public void onRefresh() {
        mPresenter.getShotById(mShot.id);
    }

    public void configureSwipeLayout() {
        mDetailSwipeLayout.setOnRefreshListener(this);
        mDetailSwipeLayout.setColorSchemeResources(R.color.material_color_control,
                R.color.primary_material,
                R.color.material_color_control,
                R.color.primary_material);
        mDetailSwipeLayout.setSize(SwipeRefreshLayout.LARGE);
    }


}
