package br.com.joaogouveia.dribbbleconcrete.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;

import br.com.joaogouveia.dribbbleconcrete.R;
import br.com.joaogouveia.dribbbleconcrete.application.DesafioApplication;
import br.com.joaogouveia.dribbbleconcrete.constants.Constants;
import br.com.joaogouveia.dribbbleconcrete.fragments.HomeFragment;

public class HomeActivity extends BaseActivity {

    public static final String FRAGMENT_TAG = "fragment:home";
    HomeFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.Extra.IS_TESTING)) {
                DesafioApplication.getInstance().setIsTesting(true);
            }
        }

        configureToolbar();
//        setToolbarTitle(R.string.);

        fragment = (HomeFragment) getFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if (fragment == null) {
            fragment = HomeFragment.newInstance();
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.base_frameLayout, fragment, FRAGMENT_TAG).commit();
    }

}
