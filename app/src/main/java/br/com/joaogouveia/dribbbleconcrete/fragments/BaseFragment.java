package br.com.joaogouveia.dribbbleconcrete.fragments;

import android.app.Fragment;
import android.os.Bundle;

import icepick.Icepick;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }
}
