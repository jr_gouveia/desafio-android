package br.com.joaogouveia.dribbbleconcrete.constants;

/**
 * Created by joaogouveia on 16/05/15.
 */
public class Constants {

    public static final String API_HOST = "https://api.dribbble.com/v1/";
    public static final String API_HOST_MOCK = "http://jsonstub.com/";
    public static final String TOKEN = "0e9849606f60f9166599d7b3c1652024079b2185098abc5c92f07a2e35b2962a";

    public static class Paths {
        public static final String SHOTS = "shots";
    }

    public static class RequestKeys {
        public static final String PAGE = "page";
        public static final String PER_PAGE = "per_page";
        public static final String LIST = "list";
        public static final String ACCESS_TOKEN = "access_token";
    }

    public static class Header {
        public static final String AUTHORIZATION = "Authorization";
    }

    public static class Parameters {
        public static final int PER_PAGE = 15;
        public static final String ANIMATED = "animated";
    }

    public class Extra {
        public static final String SHOT = "extra:shot";
        public static final String IS_TESTING = "extra:isTesting";
    }
}
